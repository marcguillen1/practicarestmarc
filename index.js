var express = require('express');

var app = express();

var db = require('./db.js');

var bodyparser = require('body-parser');

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}))

app.get('/', function (req, res) {

res.send('Hello World!');

})

app.get('/apps', function (req, res) {

	db.query("SELECT * FROM app", function(err, result){
		if(!err){
			res.json(result.rows)
		}else{
			res.send("Error a la bd");
			console.log(err);
		}
	});
})

app.get('/apps/:app_code', function (req, res) {

	db.query("SELECT * FROM records where app_code=$1",[req.params.app_code], function(err, result){
		if(result.rows.length>0){
			res.json(result.rows)
		}else{
			res.status(404);
			res.send("Error a la bd");
			console.log(err);
		}
	});
})

app.post('/apps/:app_code', function (req, res) {
	db.query("INSERT INTO records(app_code, player, score) VALUES($1, $2, $3)",[req.params.app_code, req.body.player, req.body.score], function(err, result){
		if(!err){
			res.send("correct insert");
		}else{
			res.status(500);
			res.end("Error bd")
			console.log(err);
		}
	});
})




app.listen(process.env.PORT || 3000, function () {

console.log('Example app listening on port 3000!');

})
